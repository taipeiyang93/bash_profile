# Mac OS X Terminal Aliases & How-To #

Collection of some of my fav terminal aliases that i use often & collected from the web. This file will be updated with more aliases as i find more. Feel free to comment and share your fav aliases you use :)

###Follow these simple steps to add the following aliases:###

----------

1) Within the Terminal, run:  

`vim ~/.bash_profile`

2) Type `i` and then paste the following at the top of the file:

    # Open file with Sublime Text 3 $ st /folder/example.php
    alias st='open -a "Sublime Text"'
    
    # Jump back n directories at a time
    alias ..='cd ..'
    alias ...='cd ../../'
    alias ....='cd ../../../'
    alias .....='cd ../../../../'
    alias ......='cd ../../../../../'
    
    # Flush DNS (Mac OS X Moutain Lion or Lion)
    alias dnsflush='sudo killall -HUP mDNSResponder'
    
    # To clear terminal
    alias c='clear'
    
    # To exit terminal
    alias e='exit'
    
    # See http://www.shellperson.net/using-sudo-with-an-alias/
    alias sudo='sudo '
    
    # This helps me edit files that my user isn't the owner of
    alias edit='SUDO_EDITOR="open -FWne" sudo -e'
    
    # The alias that takes me here - to editing these very aliases
    alias edit_profile='open -e ~/.bash_profile'
    
    # I do a lot of web development, so I need to edit these non-owned files fairly often
    alias edit_hosts='edit /etc/hosts'
    alias edit_httpd='edit /etc/apache2/httpd.conf'
    alias edit_php='edit /etc/php.ini'
    alias edit_vhosts='edit /etc/apache2/extra/httpd-vhosts.conf'
    
    # This alias reloads this file
    alias reload_profile='. ~/.bash_profile'
    
    # Mac get stuck very often and are extremely slow and unstable on shutdowns. This forces a shutdown.
    alias poweroff='sudo /sbin/shutdown -h now'
    
    # All other aliases
    alias la='ls -aFhlG'
    alias ll='ls -l'
    alias search=grep

    # Current path
    alias here=$(pwd)
    
    # Run second instance of Skype (Good for using multiple accounts)
    alias ss='sudo /Applications/Skype.app/Contents/MacOS/Skype /secondary'
    
    # Get OS X Software Updates, update Homebrew itself, and upgrade installed Homebrew packages
    alias update="sudo softwareupdate -i -a; brew update; brew upgrade"
    
    # Speed-up Terminal load time by clearing system logs
    alias speedup="sudo rm -rf /private/var/log/asl/*"
    
    # Empty the Trash on all mounted volumes and the main HDD
    # Also, clear Apple’s System Logs to improve shell startup speed
    alias emptytrash="sudo rm -rfv /Volumes/*/.Trashes; sudo rm -rfv ~/.Trash; speedup"
    
    # IP ADDRESSES #
    
    # To get my external IP
    alias myip='curl icanhazip.com'
    
    # Other IP / Method
    alias ip="dig +short myip.opendns.com @resolver1.opendns.com"
    alias localip="ifconfig en0 inet | grep 'inet ' | awk ' { print $2 } '"
    alias ips="ifconfig -a | perl -nle'/(\d+\.\d+\.\d+\.\d+)/ && print $1'"
    
    # Enhanced WHOIS lookups
    alias whois="whois -h whois-servers.net"
    
    # Show Hidden files in Finder
    alias finder_s='defaults write com.apple.Finder AppleShowAllFiles TRUE; killAll Finder'
    
    # Hide Hidden files in Finder
    alias finder_h='defaults write com.apple.Finder AppleShowAllFiles FALSE; killAll Finder'

    # SOME COMMANDS FOR DEVELOPER; #
    
    # Laravel Artisan
    alias art='php artisan'
    
    # Some of my goto commands, including one to open the php extension folder for when I need to install custom extensions
    alias goto_web='cd ~/Sites'
    alias goto_phpext='sudo open /usr/lib/php/extensions/no-debug-non-zts-20100525'
    
    # Go to MAMP/htdocs
    alias goto_htdocs='cd /Applications/MAMP/htdocs'
    
    # This alias recursively destroys all .DS_Store files in the folder I am currently in
    alias killDS='find . -name *.DS_Store -type f -delete'
    
    # An alias to start my custom MySQL installation instead of the default one
    alias mysql='/usr/local/mysql-5.5.25-osx10.6-x86_64/bin/mysql -u root'


3) Hit `ESC`, Type `:wq`, and hit `Enter`
    
4) Reload the bash profile, In Terminal, run:

 `source ~/.bash_profile`

And we're done! If everything went good, you should be able to start using those aliases right away :)