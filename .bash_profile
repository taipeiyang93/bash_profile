export JAVA_HOME=$(/usr/libexec/java_home)

# Setting PATH for Python 2.7
# The original version is saved in .bash_profile.pysave
PATH="/Library/Frameworks/Python.framework/Versions/2.7/bin:${PATH}"
export PATH


# This helps me edit files that my user isn't the owner of
alias edit='SUDO_EDITOR="open -FWne" sudo -e'

# Open file with Sublime Text 3 $ st /folder/example.php
alias st='open -a "Sublime Text"'

# Show/lias that takes me here - to editing these very aliases
alias edit_profile='st -e ~/.bash_profile'

# I do a lot of web development, so I need to edit these non-owned files fairly often
alias edit_hosts='edit /etc/hosts'
alias edit_httpd='edit /etc/apache2/httpd.conf'
alias edit_php='edit /etc/php.ini'
alias edit_vhosts='edit /etc/apache2/extra/httpd-vhosts.conf'

# All other aliases
alias la='ls -aFhlG'
alias ll='ls -l'
alias search=grep

# Current path
alias here='$(pwd)'

# This alias reloads this file
alias reload_profile='. ~/.bash_profile'

# Run second instance of Skype (Good for using multiple accounts)
alias ss='sudo /Applications/Skype.app/Contents/MacOS/Skype /secondary'

# Speed-up Terminal load time by clearing system logs
alias speedup='sudo rm -rf /private/var/log/asl/*'

# Print PATH environment variable in terminal. org command is "echo $PATH | sed -e $'s/:/\\\n/g'"
alias echo_path="echo $PATH | sed -e $'s/:/\\\\\\n/g'"

# IP addresses #
    
# To get my external IP
alias myip='curl icanhazip.com'

# Other IP / Method
alias ip="dig +short myip.opendns.com @resolver1.opendns.com"
alias localip="ifconfig en0 inet | grep 'inet ' | awk ' { print $2 } '"
alias ips="ifconfig -a | perl -nle'/(\d+\.\d+\.\d+\.\d+)/ && print $1'"

# Enhanced WHOIS lookups
alias whois="whois -h whois-servers.net"

# Show Hidden files in Finder
alias finder_s='defaults write com.apple.Finder AppleShowAllFiles TRUE; killAll Finder'

# Hide Hidden files in Finder
alias finder_h='defaults write com.apple.Finder AppleShowAllFiles FALSE; killAll Finder'

